package fr.esgi.music.fragments

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.adapters.AlbumListAdapter
import fr.esgi.music.adapters.TrackListAdapter
import fr.esgi.music.api.ApiManager
import fr.esgi.music.entities.Album
import fr.esgi.music.entities.Artist
import fr.esgi.music.entities.Track
import fr.esgi.music.model.AlbumModel
import fr.esgi.music.model.TrackModel
import kotlinx.android.synthetic.main.fragment_artiste.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class ArtistDetailsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_artiste, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val ArtistClicked = arguments?.get("Artist") as Artist
        val albumsArtistClicked = mutableListOf<Album>();
        val tracksArtistClicked = mutableListOf<Track>();
        lifecycleScope.launch {
            withContext(Dispatchers.Default) {
                try {
                    val responseArtist = ApiManager.getArtist(ArtistClicked.name).artists[0]
                    var artist = Artist(responseArtist.idArtist.toInt(), responseArtist.strArtist, responseArtist.strStyle, responseArtist.intBornYear, responseArtist.strArtistThumb, responseArtist.strBiographyFR, responseArtist.strBiographyEN)

                    val responseAlbums = ApiManager.getAlbumsByArtist(artist.id.toString()).album
                    for(album: AlbumModel in responseAlbums) {
                        val albumArtistClicked = Album(album.idAlbum.toInt(), 0, album.strAlbum, album.strArtist, album.strArtist, album.intYearReleased, "randomAlbum.strDescriptionFR", "randomAlbum.strDescriptionEN", album.strAlbumThumb, false);
                        albumsArtistClicked += albumArtistClicked
                    }

                    val responseTracks = ApiManager.getBestArtistTracks(ArtistClicked.name).track
                    var numero = 1;
                    for(track: TrackModel in responseTracks) {
                        val trackArtistClicked = Track(track.idAlbum.toInt(), numero.toString(), track.strTrack, track.strArtist,track.strTrackThumb)
                        tracksArtistClicked += trackArtistClicked
                        numero++
                    }
                    bindArtist(artist)
                    val albumRecyclerView =
                        view.findViewById<RecyclerView>(R.id.album_recycler_view_w)
                    albumRecyclerView.adapter = AlbumListAdapter(albumsArtistClicked, requireActivity())

                    val titleRecyclerView =
                        view.findViewById<RecyclerView>(R.id.title_recycler_view_w)
                    titleRecyclerView.adapter = TrackListAdapter(tracksArtistClicked, requireActivity())
                } catch (e: Exception) {
                    println("*ERROR*"+ e)
                }
            }

        }

        val retour = view.findViewById<ImageView>(R.id.retour_Artist)

        retour.setOnClickListener {
            val fragmentManager: FragmentManager = (activity as AppCompatActivity).supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content, BottomNavFragment()).addToBackStack("")
                .commitAllowingStateLoss();
        }
    }

    @Throws(IOException::class)
    fun drawableFromUrl(url: String?): Drawable? {
        val x: Bitmap
        val connection: HttpURLConnection = URL(url).openConnection() as HttpURLConnection
        connection.connect()
        val input: InputStream = connection.getInputStream()
        x = BitmapFactory.decodeStream(input)
        return BitmapDrawable(Resources.getSystem(), x)
    }

    fun bindArtist(Artist: Artist) {
        Artist_name_details.text = Artist.name
        biographieArtist.text = Artist.bioFrench
        image_Artist_details.setImageDrawable(drawableFromUrl(Artist.urlPhoto))
    }
}