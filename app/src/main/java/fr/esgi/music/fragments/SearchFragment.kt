package fr.esgi.music.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.adapters.AlbumListAdapter
import fr.esgi.music.adapters.ArtistListAdapter
import fr.esgi.music.api.ApiManager
import fr.esgi.music.entities.Album
import fr.esgi.music.entities.Artist
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater?.inflate(R.layout.fragment_search, container, false)

        val albums = mutableListOf<Album>();
        var artists =  mutableListOf<Artist>()




        lifecycleScope.launch {
            var editTextRecherche= view.findViewById<EditText>(R.id.editText)
            withContext(Dispatchers.Default) {

            editTextRecherche.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {

                }

                override fun beforeTextChanged(s: CharSequence, start: Int,
                                               count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence, start: Int,
                                           before: Int, count: Int) {
                    val artistRetrieved = editTextRecherche.text.toString()
                    lifecycleScope.launch {
                    withContext(Dispatchers.Default) {
                        try {
                            val responseAdele = ApiManager.getArtist(artistRetrieved).artists[0]
                            val Artist = Artist(responseAdele.idArtist.toInt(), responseAdele.strArtist, responseAdele.strStyle, responseAdele.intBornYear, responseAdele.strArtistThumb, responseAdele.strBiographyFR, responseAdele.strBiographyEN)
                            artists = mutableListOf(Artist)
                                val randomAlbum = ApiManager.getAlbumsByArtist(Artist.id.toString()).album[0]
                                val album = Album(randomAlbum.idAlbum.toInt(), 0, randomAlbum.strAlbum, randomAlbum.strArtist, randomAlbum.strArtist, randomAlbum.intYearReleased, if (randomAlbum.strDescriptionFR == null) "No description" else randomAlbum.strDescriptionFR, if (randomAlbum.strDescriptionEN == null) "No description" else randomAlbum.strDescriptionEN, randomAlbum.strAlbumThumb, false);
                            val ArtistsRecyclerView =
                                view.findViewById<RecyclerView>(R.id.Artists_recherche_recycler_view)
                            ArtistsRecyclerView.adapter = ArtistListAdapter(artists, requireActivity())


                        } catch (e: Exception) {
                            println("*ERROR*"+ e)
                        }
                    }}
                }
            })
            withContext(Dispatchers.Default) {
                try {
                    val responseEminem = ApiManager.getArtist("eminem").artists[0]
                    val eminem = Artist(responseEminem.idArtist.toInt(), responseEminem.strArtist, responseEminem.strStyle, responseEminem.intBornYear, responseEminem.strArtistThumb, responseEminem.strBiographyFR, responseEminem.strBiographyEN)
                    artists += eminem

                    val responseAliciaK= ApiManager.getArtist("Dr. Dre").artists[0]
                    val drDre = Artist(responseAliciaK.idArtist.toInt(), responseAliciaK.strArtist, responseAliciaK.strStyle, responseAliciaK.intBornYear, responseAliciaK.strArtistThumb, responseAliciaK.strBiographyFR, responseAliciaK.strBiographyEN)
                    artists += drDre

                    for(Artist: Artist in artists) {
                        val randomAlbum = ApiManager.getAlbumsByArtist(Artist.id.toString()).album[0]
                        val album = Album(randomAlbum.idAlbum.toInt(), 0, randomAlbum.strAlbum, randomAlbum.strArtist, randomAlbum.strArtist, randomAlbum.intYearReleased, if (randomAlbum.strDescriptionFR == null) "No description" else randomAlbum.strDescriptionFR, if (randomAlbum.strDescriptionEN == null) "No description" else randomAlbum.strDescriptionEN, randomAlbum.strAlbumThumb, false);
                        albums += album
                    }
                } catch (e: Exception) {
                    println("*ERROR*"+ e)
                }
            }}

            val ArtistsRecyclerView =
                view.findViewById<RecyclerView>(R.id.Artists_recherche_recycler_view)
            ArtistsRecyclerView.adapter = ArtistListAdapter(artists, requireActivity())

            val albumRecyclerView =
            view.findViewById<RecyclerView>(R.id.albums_recherche_recycler_view)
            albumRecyclerView.adapter = AlbumListAdapter(albums, requireActivity())
        }
        return view
    }


}