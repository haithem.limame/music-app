package fr.esgi.music.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import fr.esgi.music.R
import fr.esgi.music.adapters.RankingAlbumListAdapter
import fr.esgi.music.adapters.RankingTrackListAdapter
import fr.esgi.music.api.ApiManager
import fr.esgi.music.entities.Album
import fr.esgi.music.entities.Track
import fr.esgi.music.model.AlbumModel
import fr.esgi.music.model.TrackModel
import kotlinx.android.synthetic.main.fragment_ranking_albums.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AlbumsRankingFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

            val view = inflater?.inflate(R.layout.fragment_ranking_albums, container, false)
            val albums = mutableListOf<Album>()
                lifecycleScope.launch {
                    withContext(Dispatchers.Default) {
                        try {
                            val bestAlbums = ApiManager.getAlbumRank().loved
                            var numero = 1
                            for (album: AlbumModel in bestAlbums) {
                                val artistAlbum = Album(
                                    album.idAlbum.toInt(),
                                    numero,
                                    album.strAlbum,
                                    album.strArtist,
                                    album.strArtist,
                                    album.intYearReleased,
                                    "album.strDescriptionFR",
                                    "album.strDescriptionEN",
                                    album.strAlbumThumb,
                                    false
                                );
                                albums += artistAlbum
                                numero++
                            }
                        } catch (e: Exception) {
                            println("*ERROR*" + e)
                        }
                    }

                    val albumrankRecyclerView =
                        view.findViewById<RecyclerView>(R.id.albums_rank_recycler_view)
                    albumrankRecyclerView.adapter =
                        RankingAlbumListAdapter(albums!!.take(11), requireActivity())

                }

            return view
        }


}