package fr.esgi.music.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.adapters.RankingTrackListAdapter
import fr.esgi.music.api.ApiManager
import fr.esgi.music.entities.Album
import fr.esgi.music.entities.Track
import fr.esgi.music.model.TrackModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RankingTracksFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater?.inflate(R.layout.fragment_ranking_tracks, container, false)

        val tracks = mutableListOf<Track>()

        lifecycleScope.launch {
            withContext(Dispatchers.Default) {
                try {
                    val bestTracks = ApiManager.getTrackRank().loved
                    var numero = 1
                    for (track: TrackModel in bestTracks) {
                        val song = Track(
                            track.idAlbum.toInt(),
                            track.strTrack,
                            track.idArtist,
                            track.strArtist,
                            track.strTrackThumb
                        )
                        numero++
                        tracks += song
                    }
                } catch (e: Exception) {
                    println("*ERROR*" + e)
                }
            }

            // recuperer le recyclerview
            val rankTitleRecyclerView =
                view.findViewById<RecyclerView>(R.id.Tracks_rank_recycler_view)
            rankTitleRecyclerView.adapter = RankingTrackListAdapter(tracks!!.take(11), requireActivity())

        }

        return view
    }
}