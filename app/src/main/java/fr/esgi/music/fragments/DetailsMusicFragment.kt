package fr.esgi.music.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import fr.esgi.music.R
import fr.esgi.music.entities.Album
import fr.esgi.music.entities.Artist
import fr.esgi.music.entities.Track
import kotlinx.android.synthetic.main.fragment_bottom_navigation_view.*

class DetailsMusicFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_bottom_navigation_view,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val navHost = childFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        NavigationUI.setupWithNavController(bottom_nav, navHost.navController)
    }


    companion object {
        fun newInstance(artist: Artist) : ArtistDetailsFragment {
            val fragment = ArtistDetailsFragment()
            val args = bundleOf("artist" to artist)
            fragment.arguments = args
            return fragment
        }

        fun newInstance(album: Album) : AlbumDetailsFragment {
            val fragment = AlbumDetailsFragment()
            val args = bundleOf("album" to album)
            fragment.arguments = args
            return fragment
        }

        fun newInstance(track: Track) : TrackDetailsFragment {
            val fragment = TrackDetailsFragment()
            val args = bundleOf("track" to track)
            fragment.arguments = args
            return fragment
        }
    }
}