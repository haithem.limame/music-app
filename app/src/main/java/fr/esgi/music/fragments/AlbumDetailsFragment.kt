package fr.esgi.music.fragments

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.adapters.TrackListAdapter
import fr.esgi.music.api.ApiManager
import fr.esgi.music.entities.Album
import fr.esgi.music.entities.Track
import fr.esgi.music.model.TrackModel
import kotlinx.android.synthetic.main.fragment_album.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class AlbumDetailsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_album, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val albumClicked = arguments?.get("album") as Album
        val tracksAlbumClicked = mutableListOf<Track>();
        lifecycleScope.launch {
            var album = Album(1,1, " ", " ", " ", "1", "", "", " ", false)
            withContext(Dispatchers.Default) {
                try {
                    val responseAlbum = ApiManager.getAlbumByArtistAndAlbumName(albumClicked.artistName, albumClicked.title).album[0]
                    val descriptionEmpty = getString(R.string.empty_description)
                    val numeroAlbum = 0

                    album = Album(responseAlbum.idAlbum.toInt(), numeroAlbum, responseAlbum.strAlbum, responseAlbum.strArtist, responseAlbum.strArtist, responseAlbum.intYearReleased, if (responseAlbum.strDescriptionFR == null) descriptionEmpty.toString() else responseAlbum.strDescriptionFR, if (responseAlbum.strDescriptionEN == null) descriptionEmpty.toString() else responseAlbum.strDescriptionEN, responseAlbum.strAlbumThumb, false);

                    val responseTracks = ApiManager.getTracksByAlbum(albumClicked.id.toString()).track
                    var numerotitle = 1;
                    for(track: TrackModel in responseTracks) {
                        val trackAlbumClicked = Track(track.idAlbum.toInt(), numerotitle.toString(), track.strTrack, track.strArtist,track.strTrackThumb)
                        tracksAlbumClicked += trackAlbumClicked
                        numerotitle++
                    }

                } catch (e: Exception) {
                    println("*ERROR*"+ e)
                }

            }
            bindAlbum(album)

            val titleRecyclerView =
                view.findViewById<RecyclerView>(R.id.album_recycler_view_w)
            titleRecyclerView.adapter = TrackListAdapter(tracksAlbumClicked, requireActivity())

        }
        val retour = view.findViewById<ImageView>(R.id.retour_album)

        retour.setOnClickListener {
            val fragmentManager: FragmentManager = (activity as AppCompatActivity).supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content, BottomNavFragment()).addToBackStack("")
                .commitAllowingStateLoss();
        }
    }

    @Throws(IOException::class)
    fun drawableFromUrl(url: String?): Drawable? {
        val x: Bitmap
        val connection: HttpURLConnection = URL(url).openConnection() as HttpURLConnection
        connection.connect()
        val input: InputStream = connection.getInputStream()
        x = BitmapFactory.decodeStream(input)
        return BitmapDrawable(Resources.getSystem(), x)
    }

    fun bindAlbum(album: Album) {
        title_details_album.text = album.title
        Artist_name_details_album.text = album.artistName
        descriptionAlbum.text = album.descriptionFrench

        image_album_details.setImageDrawable(drawableFromUrl(album.urlPhoto))
        image_album_details.alpha = 0.95f
        image_Artist_album.setImageDrawable(drawableFromUrl(album.urlPhoto))
    }
}