package fr.esgi.music.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.adapters.AlbumListAdapter
import fr.esgi.music.adapters.ArtistListAdapter
import fr.esgi.music.api.ApiManager
import fr.esgi.music.entities.Album
import fr.esgi.music.entities.Artist
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavoritesFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater?.inflate(R.layout.fragment_favorites, container, false)


        val albums = mutableListOf<Album>();
        val artists =  mutableListOf<Artist>()

        lifecycleScope.launch {
            withContext(Dispatchers.Default) {
                try {
                    val responseAdele = ApiManager.getArtist("2pac").artists[0]
                    val pac = Artist(responseAdele.idArtist.toInt(), responseAdele.strArtist, responseAdele.strStyle, responseAdele.intBornYear, responseAdele.strArtistThumb, responseAdele.strBiographyFR, responseAdele.strBiographyEN)
                    artists += pac

                    val responseSia= ApiManager.getArtist("eminem").artists[0]
                    val eminem = Artist(responseSia.idArtist.toInt(), responseSia.strArtist, responseSia.strStyle, responseSia.intBornYear, responseSia.strArtistThumb, responseSia.strBiographyFR, responseSia.strBiographyEN)
                    artists += eminem

                    for(Artist: Artist in artists) {
                        val randomAlbum = ApiManager.getAlbumsByArtist(Artist.id.toString()).album[0]
                        val album = Album(randomAlbum.idAlbum.toInt(), 0, randomAlbum.strAlbum, randomAlbum.strArtist, randomAlbum.strArtist, randomAlbum.intYearReleased, if (randomAlbum.strDescriptionFR == null) "No description" else randomAlbum.strDescriptionFR, if (randomAlbum.strDescriptionEN == null) "No description" else randomAlbum.strDescriptionEN, randomAlbum.strAlbumThumb, false);
                        albums += album
                    }

                } catch (e: Exception) {
                    println("*ERROR*"+ e)
                }
            }
            // recuperer le recyclerview
            val ArtistsRecyclerView =
                view.findViewById<RecyclerView>(R.id.Artists_favoris_recycler_view)
            ArtistsRecyclerView.adapter = ArtistListAdapter(artists, requireActivity())

            val albumRecyclerView =
                view.findViewById<RecyclerView>(R.id.albums_favoris_recycler_view)
            albumRecyclerView.adapter = AlbumListAdapter(albums, requireActivity())
        }
        return view
    }

}