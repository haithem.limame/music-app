package fr.esgi.music.holders

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.entities.Artist
import fr.esgi.music.fragments.DetailsMusicFragment
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class ArtistViewHolder(v: View, a: FragmentActivity) : RecyclerView.ViewHolder(v) {

    val name = v.findViewById<TextView>(R.id.Artist_name_rank)
    val card = v.findViewById<CardView>(R.id.card_Artist)
    val photo = v.findViewById<ImageView>(R.id.photo_Artist_rank)
    var activity = a

    fun bindArtist(Artist: Artist) {
        name.text = Artist.name
        photo.setImageDrawable(drawableFromUrl(Artist.urlPhoto))
        card.setOnClickListener {
            val fragmentManager: FragmentManager = (activity as AppCompatActivity).supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content, DetailsMusicFragment.newInstance(Artist)).addToBackStack("")
                .commitAllowingStateLoss();
        }
    }

    @Throws(IOException::class)
    fun drawableFromUrl(url: String?): Drawable? {
        val x: Bitmap
        val connection: HttpURLConnection = URL(url).openConnection() as HttpURLConnection
        connection.connect()
        val input: InputStream = connection.getInputStream()
        x = BitmapFactory.decodeStream(input)
        return BitmapDrawable(Resources.getSystem(), x)
    }

}