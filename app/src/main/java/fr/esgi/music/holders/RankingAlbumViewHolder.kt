package fr.esgi.music.holders

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.entities.Album
import fr.esgi.music.fragments.DetailsMusicFragment
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class RankingAlbumViewHolder(v: View, a: FragmentActivity) : RecyclerView.ViewHolder(v) {
    val title = v.findViewById<TextView>(R.id.title_rank)
    val artistName = v.findViewById<TextView>(R.id.album_Artist_name_rank)
    val card = v.findViewById<FrameLayout>(R.id.card)
    val photo = v.findViewById<ImageView>(R.id.image_title_rank)
    val rank = v.findViewById<TextView>(R.id.track_number_rank)
    var activity = a

    fun bindAlbum(album: Album) {
        title.text = album.title
        artistName.text = album.artistName
        photo.setImageDrawable(drawableFromUrl(album.urlPhoto))
        rank.text = album.rank.toString()
        card.setOnClickListener {
            val fragmentManager: FragmentManager = (activity as AppCompatActivity).supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content, DetailsMusicFragment.newInstance(album)).addToBackStack("")
                .commitAllowingStateLoss();
        }
    }

    @Throws(IOException::class)
    fun drawableFromUrl(url: String?): Drawable? {
        val x: Bitmap
        val connection: HttpURLConnection = URL(url).openConnection() as HttpURLConnection
        connection.connect()
        val input: InputStream = connection.getInputStream()
        x = BitmapFactory.decodeStream(input)
        return BitmapDrawable(Resources.getSystem(), x)
    }
}