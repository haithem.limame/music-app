package fr.esgi.music.holders

import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.entities.Track
import fr.esgi.music.fragments.DetailsMusicFragment

class TrackViewHolder(v: View, a: FragmentActivity) : RecyclerView.ViewHolder(v) {

    val rank = v.findViewById<TextView>(R.id.track_number_Artist_details)
    val rankTitle = v.findViewById<TextView>(R.id.title_rank)
    val card = v.findViewById<FrameLayout>(R.id.item_title_apprecie)
    var activity = a

    fun bindtitle(track: Track) {
        rank.text = track.rank
        rankTitle.text = track.title


        card.setOnClickListener {
            val fragmentManager: FragmentManager = (activity as AppCompatActivity).supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content, DetailsMusicFragment.newInstance(track)).addToBackStack("")
                .commitAllowingStateLoss();
        }
    }
}