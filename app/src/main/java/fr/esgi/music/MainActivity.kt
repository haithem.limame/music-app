package fr.esgi.music

import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import androidx.appcompat.app.AppCompatActivity
import fr.esgi.music.fragments.BottomNavFragment


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        supportFragmentManager.beginTransaction()
        .replace(R.id.content,BottomNavFragment())
            .commitAllowingStateLoss()

    }


}