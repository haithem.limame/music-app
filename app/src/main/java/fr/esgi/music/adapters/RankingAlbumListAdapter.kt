package fr.esgi.music.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.entities.Album
import fr.esgi.music.holders.RankingAlbumViewHolder

class  RankingAlbumListAdapter(val albums : List<Album>, val fragmentActivity: FragmentActivity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return RankingAlbumViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_simple, parent, false), fragmentActivity
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = holder as RankingAlbumViewHolder
        val album = albums[position]
        item.bindAlbum(album)

    }

    override fun getItemCount(): Int = albums.size
}