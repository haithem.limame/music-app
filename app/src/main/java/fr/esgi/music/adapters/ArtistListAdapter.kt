package fr.esgi.music.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.entities.Artist
import fr.esgi.music.holders.ArtistViewHolder

class ArtistListAdapter(val artists : List<Artist>, val fragmentActivity: FragmentActivity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return ArtistViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_artist, parent, false), fragmentActivity
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = holder as ArtistViewHolder
        item.bindArtist(artists[position])
    }

    override fun getItemCount(): Int = artists.size
}