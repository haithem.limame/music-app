package fr.esgi.music.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import fr.esgi.music.R
import fr.esgi.music.entities.Track
import fr.esgi.music.holders.TrackViewHolder

class TrackListAdapter (val tracks : List<Track>, val fragmentActivity: FragmentActivity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return TrackViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_track_details, parent, false), fragmentActivity
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = holder as TrackViewHolder
        item.bindtitle(tracks[position])
    }

    override fun getItemCount(): Int = tracks.size
}