package fr.esgi.music.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Artist(
    val id : @RawValue Int,
    val name : @RawValue String,
    val style: @RawValue String,
    val birthYear: @RawValue String,
    val urlPhoto: @RawValue String,
    val bioFrench: @RawValue String,
    val bioEnglish: @RawValue String,
    ): Parcelable {
}
