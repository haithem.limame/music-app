package fr.esgi.music.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Track(
    val id : @RawValue Int,
    val rank : @RawValue String,
    val title : @RawValue String,
    val artistName : @RawValue String,
    val urlPhoto : @RawValue String,
    ): Parcelable {

}