package fr.esgi.music.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Album(
    val id : @RawValue Int,
    val rank : @RawValue Int,
    val title : @RawValue String,
    val idArtist : @RawValue String,
    val artistName : @RawValue String,
    val year: @RawValue String,
    val descriptionFrench: @RawValue String,
    val descriptionEnglish: @RawValue String,
    val urlPhoto: @RawValue String,
    val liked : @RawValue Boolean,
): Parcelable {

}
