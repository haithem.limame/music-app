package fr.esgi.music.model

data class ArtistModel(
    val idArtist: String,
    val strArtist: String,
    val strStyle: String,
    val intBornYear: String,
    val strArtistThumb: String,
    val strBiographyFR: String,
    val strBiographyEN: String,
)
