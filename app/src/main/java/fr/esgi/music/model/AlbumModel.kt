package fr.esgi.music.model

data class AlbumModel(
    val idAlbum: String,
    val strAlbum: String,
    val idArtist: String,
    val strArtist: String,
    val intYearReleased: String,
    val strDescriptionFR: String,
    val strDescriptionEN: String,
    val strAlbumThumb: String
)
