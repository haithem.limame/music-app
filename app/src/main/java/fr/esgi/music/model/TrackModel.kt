package fr.esgi.music.model

data class TrackModel (
    val idTrack: String,
    val strTrack: String,
    val idAlbum: String,
    val strAlbum: String,
    val idArtist: String,
    val strArtist: String,
    val strTrackThumb: String,
)