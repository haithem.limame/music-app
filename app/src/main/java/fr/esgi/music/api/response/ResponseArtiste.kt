package fr.esgi.music.api.response

import fr.esgi.music.model.ArtistModel

data class ResponseArtist(
    val artists: List<ArtistModel>
)
