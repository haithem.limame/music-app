package fr.esgi.music.api.response

import fr.esgi.music.model.TrackModel

data class ResponseTrack (
    val track: List<TrackModel>
    )