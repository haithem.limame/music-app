package fr.esgi.music.api.response

import fr.esgi.music.model.AlbumModel

data class ResponseLovedAlbum (
    val loved: List<AlbumModel>
)