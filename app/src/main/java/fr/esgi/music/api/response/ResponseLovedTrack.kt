package fr.esgi.music.api.response

import fr.esgi.music.model.TrackModel

data class ResponseLovedTrack (
    val loved: List<TrackModel>
    )