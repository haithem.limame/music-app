package fr.esgi.music.api

import com.example.requesttest.request.API
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import fr.esgi.music.api.response.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
private const val BASE_URL = "https://www.theaudiodb.com/api/v1/json/523532/"
object ApiManager {
    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
        .create(API::class.java)
    //Artist
    suspend fun getBestArtistTracks(artist:String) : ResponseTrack {
        return api.getBestArtistTracks(artist).await()
    }

    suspend fun getArtist(artist: String) : ResponseArtist {
        return api.getArtistByName(artist).await()
    }
    //Album
    suspend fun getAlbumRank() : ResponseLovedAlbum {
        return api.getAlbumRank("album").await()
    }

    suspend fun getAlbumsByArtist(artistId:String) : ResponseAlbum {
        return api.getAlbumsByArtist(artistId).await()
    }

    suspend fun getAlbumByArtistAndAlbumName(artist: String, album: String) : ResponseAlbum {
        return api.getAlbumByArtistAndAlbumName(artist, album).await()
    }
    //Track
    suspend fun getTrackRank() : ResponseLovedTrack {
        return api.getTrackRank("track").await()
    }

    suspend fun getTracksByAlbum(albumId: String) : ResponseTrack {
        return api.getTracksByAlbum(albumId).await()
    }

}