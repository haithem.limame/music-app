package fr.esgi.music.api.response

import fr.esgi.music.model.AlbumModel

data class ResponseAlbum (

    val album: List<AlbumModel>
)