package com.example.requesttest.request

import fr.esgi.music.api.response.*
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query


interface API {
    @GET("search.php")
    fun getArtistByName(@Query("s") artist: String): Deferred<ResponseArtist>

    @GET("album.php")
    fun getAlbumsByArtist(@Query("i") artistId: String): Deferred<ResponseAlbum>

    @GET("searchalbum.php")
    fun getAlbumByArtistAndAlbumName(@Query("s") artist: String, @Query("a") album: String): Deferred<ResponseAlbum>

    @GET("mostloved.php")
    fun getAlbumRank(@Query("format") format: String): Deferred<ResponseLovedAlbum>

    @GET("mostloved.php")
    fun getTrackRank(@Query("format") format: String): Deferred<ResponseLovedTrack>

    @GET("track.php")
    fun getTracksByAlbum(@Query("m") albumId: String): Deferred<ResponseTrack>

    @GET("track-top10.php")
    fun getBestArtistTracks(@Query("s") albumId: String): Deferred<ResponseTrack>
}